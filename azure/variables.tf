variable "AZ_CLIENT_ID" {
  description = "Terraform client id for service principal"
  type        = string
}

variable "AZ_SECRET" {
  description = "Terraform secret for service principal"
  type        = string
}

variable "AZ_SUBSCRIPTION_ID" {
  description = "Terraform subscription if for service principal"
  type        = string
}

variable "AZ_TENANT_ID" {
  description = "Terraform subscription if for service principal"
  type        = string
}


terraform {
  cloud {
    organization = "dandejager"

    workspaces {
      name = "Azure_Workspace"
    }
  }


  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.65"
    }
  }

  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}

  subscription_id = var.AZ_SUBSCRIPTION_ID
  client_id       = var.AZ_CLIENT_ID
  client_secret   = var.AZ_SECRET
  tenant_id       = var.AZ_TENANT_ID

}

resource "azurerm_resource_group" "rg" {
  name     = "myTFResourceGroup"
  location = "australiaeast"
  tags = {
      Environment = "Terrform Getting Started"
      Team = "DevOps"
  }
}

resource "azurerm_virtual_network" "vnet" {
  name = "myTFvnet"
  address_space = ["10.0.0.0/16"]
  location = "australiaeast"
  resource_group_name = azurerm_resource_group.rg.name
}
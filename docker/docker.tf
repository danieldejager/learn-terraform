terraform {

  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.16.0"
    }
  }
}

#configure the docker provider 
provider "docker" {

}

#create a docker image resource 
resource "docker_image" "nginx" {
  name         = "nginx:latest"
  keep_locally = "false"
}

resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "tutorial"
  ports {
    external = 8080
    internal = 80
    ip       = "127.0.0.1"
  }
}
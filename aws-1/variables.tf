variable "instance_name" {
    description = "Value of the name tag for the EC2 instance"
    type = string 
    default = "ExampleAppServerInstance"
}

variable "AWS_SECRET_ACCESS_KEY" {
    description = "Value of the AWS secret"
    type = string
}

variable "AWS_ACCESS_KEY_ID" {
    description = "Value of the AWS secret"
    type = string
}


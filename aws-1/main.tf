terraform {
    cloud {
        organization = "ddjsandbox"

    workspaces {
      name = "sandbox"
    }
}



    required_providers {
      aws = {
        source = "hashicorp/aws"
        version = "~> 3.27"
      }
    }

    required_version = ">=0.14.9"
}

provider aws {
    profile = "default"
    region = "ap-southeast-2"
    secret_key = var.AWS_SECRET_ACCESS_KEY
    access_key = var.AWS_ACCESS_KEY_ID
}   

resource "aws_instance" "app_server" {
    ami = "ami-0672b175139a0f8f4"
    instance_type = "t2.micro"

    tags = {
        Name= var.instance_name
    }
 }
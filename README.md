# Learn Terraform

Repository with all the code whilst learning Terraform 

## Description
Terraform is a **declarative** language that is used to provision and maintain infrastructure on multiple platforms such as GCP, AWS and Azure etc. This repo contains the public tutorials that you can find on the hashicorp site. Simply pull this repo and edit away - I might suggest that you get a Hashicorp Cloud account so that you can store your secrets over there instead of the tf files. NOTE - there are no secrets in this repo, so bad luck, bad guys!

## Support
There is no support, however I can recommend a mentor in the form of Brad McCoy on this slack group --> cloudnative-mentoring.slack.com 

## Contributing
There is no reason for you to contribute at this point in time, but happy if you want to. 
